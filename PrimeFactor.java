import java.util.Scanner;

public class PrimeFactor {

	public static void primfaktor(long psZahl) { //Methode zur Bestimmung der Primfaktoren
		//Bei Aufruf der Methode wird ein Primfaktor zurueckgegeben
		//der "Rest" wird rekursiv weiter untersucht. 
		for(long psPrimfaktor = 2; psPrimfaktor <= psZahl; psPrimfaktor++) { //Probiert verschiedene moegliche Primfaktoren aus
			if(psZahl % psPrimfaktor == 0) { // Wenn ein Primfaktor gefunden ist, wird er ausgegeben und mit dem "Restwert" weitergerechnet
				long ergebnis = (psZahl / psPrimfaktor);
				System.out.print(psPrimfaktor + " ");
				primfaktor(ergebnis); 
			}
			if(psPrimfaktor == psZahl) { // Ist er prim, endet das Programm
				System.exit(0); 
			}
		}

		//Ruckgabe des aktuell bestimmten Primfaktors
		//return teiler;
		
	}
	
	public static void main(String[] args) {
		
			try (Scanner input = new Scanner(System.in)) { //Umgebung zur Annahme von Eingaben
			
				System.out.println("Zahl eingeben (maximal |9223372036854775807|)"); //Bittet um die Eingabe der Zahl
			
				long zahl = input.nextLong(); //Nimmt die Eingabe der Zahl entgegen und schreibt sie in die Variable zahl
			
				System.out.print("Primfaktor(en) von " + zahl + ": "); //Gibt den ersten Teil des Resultats aus
				
				primfaktor(zahl); // Ruft die Methode zur Primfaktorbestimmung auf
			}

	}

}